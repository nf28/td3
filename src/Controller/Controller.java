package Controller;

import Model.Contact;
import Model.Country;
import Model.Model;
import Model.Group;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.System.out;

public class Controller {

    private Contact contact;
    private Model model;

    @FXML
    private TextField firstName = new TextField();
    @FXML
    private TextField lastName = new TextField();
    @FXML
    private TextField addressName = new TextField();
    @FXML
    private TextField postcode = new TextField();
    @FXML
    private TextField city = new TextField();
    @FXML
    private ComboBox<String> countryComboBox;
    @FXML
    private DatePicker birthDate = new DatePicker();
    @FXML
    private ToggleGroup sex;
    @FXML
    private RadioButton male = new RadioButton();
    @FXML
    private RadioButton female = new RadioButton();
    @FXML
    private Button validate;
    @FXML
    private VBox contactPane;
    @FXML
    private MenuItem save;
    @FXML
    private MenuItem load;

    private MapChangeListener<String,String> listener;

    private HashMap<String, Control> controlsMap;

    private ListChangeListener<Group> groupsListener;

    // groups
    @FXML
    private TreeView<Object> tree;
    @FXML
    private Button add = new Button();
    @FXML
    private Button remove = new Button();

    public Controller () {
        this.contact = new Contact();
        this.model = new Model();
        this.controlsMap = new HashMap<String, Control>();
        this.tree = new TreeView<Object>();
    }

    @FXML
    public void initialize(){

        // Hide contactPane
        contactPane.visibleProperty().set(false);

        // Initialize countries
        countryComboBox.setItems(Country.getCountries());

        // Binding view -> model
        this.firstName.textProperty().bindBidirectional(this.contact.firstNameProperty());
        this.lastName.textProperty().bindBidirectional(this.contact.lastNameProperty());
        this.addressName.textProperty().bindBidirectional(this.contact.getAddress().addressNameProperty());
        this.postcode.textProperty().bindBidirectional(this.contact.getAddress().postcodeProperty());
        this.city.textProperty().bindBidirectional(this.contact.getAddress().cityProperty());

        this.countryComboBox.getSelectionModel().selectedItemProperty().addListener((obs, oldV, newV) -> {
            this.contact.getAddress().setCountry(newV);
        });

        this.sex.selectedToggleProperty().addListener((obs, oldV, newV) -> {
            RadioButton radio = (RadioButton) sex.getSelectedToggle();
            if (radio != null) {
                this.contact.setSex(radio.getText());
            }
        });

        this.birthDate.valueProperty().addListener((obs, oldV, newV) -> {
            this.contact.setBirthDate(newV);
        });

        // validation contact
        this.controlsMap.put(this.contact.firstNameProperty().getName(), this.firstName);
        this.controlsMap.put(this.contact.lastNameProperty().getName(), this.lastName);
        this.controlsMap.put(this.contact.getAddress().addressNameProperty().getName(), this.addressName);
        this.controlsMap.put(this.contact.getAddress().postcodeProperty().getName(), this.postcode);
        this.controlsMap.put(this.contact.getAddress().cityProperty().getName(), this.city);
        this.controlsMap.put(this.contact.getAddress().countryProperty().getName(), this.countryComboBox);
        this.controlsMap.put(this.contact.birthDateProperty().getName(), this.birthDate);

        listener = changed -> {
            if(changed.wasAdded()) {
                if(changed.getKey() == this.contact.sexProperty().getName()) {
                    this.male.setStyle("-fx-border-color: red ;");
                    this.female.setStyle("-fx-border-color: red ;");
                    this.male.setTooltip(new Tooltip(changed.getValueAdded()));
                    this.female.setTooltip(new Tooltip(changed.getValueAdded()));
                } else {
                    this.controlsMap.get(changed.getKey()).setStyle("-fx-border-color: red ;");
                    this.controlsMap.get(changed.getKey()).setTooltip(new Tooltip(changed.getValueAdded()));
                }
            } else {
                if(changed.getKey() == this.contact.sexProperty().getName()) {
                    this.male.setStyle("-fx-border-color: null ;");
                    this.female.setStyle("-fx-border-color: null ;");
                    this.male.setTooltip(null);
                    this.female.setTooltip(null);
                } else{
                this.controlsMap.get(changed.getKey()).setStyle("-fx-border-color: null ;");
                this.controlsMap.get(changed.getKey()).setTooltip(null);
                }
            }
        };

        this.contact.getErrors().addListener(listener);

        this.validate.setOnAction(evt -> {

            // Validate fields
            if(!this.contact.validate()) {
                return;
            }

            // Check if selected node is a group
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
            Object selectedItemValue = selectedItem.getValue();
            if (selectedItemValue instanceof Group) {

                // Create new contact
                try {
                    Contact newContact = (Contact) contact.clone();
                    ((Group) selectedItemValue).getContacts().add(newContact);

                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }

            // If selected node is a contact : edition
            else if (selectedItemValue instanceof Contact) {
                Group parent = (Group) selectedItem.getParent().getValue();
                out.println(parent.toString());
                parent.getContacts().remove(selectedItemValue);
                try {
                    Contact newContact = (Contact) contact.clone();
                    parent.getContacts().add(newContact);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }

        });

        // Handle groups
        TreeItem<Object> rootNode;
        rootNode = new TreeItem<Object>("Fiche de contacts");
        rootNode.setExpanded(true);
        tree.setRoot(rootNode);
        tree.setEditable(false);

        // Contacts listener
        ListChangeListener<Contact> contactsListener = change -> {
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
            change.next();
            if (change.wasAdded()) {
                change.getAddedSubList().forEach(c -> {
                    TreeItem<Object> newContact = new TreeItem(c, new ImageView((new Image("Images/contact.png"))));

                    EventHandler<MouseEvent> mouseEventEventHandler = (MouseEvent event) -> {
                        Object selectedItemValue = tree.getSelectionModel().getSelectedItem().getValue();
                        if (selectedItemValue instanceof  Contact) {
                            Contact contact = (Contact) selectedItemValue;
                            this.firstName.setText(contact.getFirstName());
                            this.lastName.setText(contact.getLastName());
                            this.birthDate.getEditor().setText(contact.getBirthDate().toString());
                            this.postcode.setText((contact.getAddress().getPostcode()));
                            this.addressName.setText(contact.getAddress().getAddressName());
                            this.city.setText(contact.getAddress().getCity());
                            this.countryComboBox.setValue(contact.getAddress().getCountry());

                            String sex = contact.getSex();
                            if (sex.equals("M")) {
                                this.male.setSelected(true);
                                this.female.setSelected(false);
                            } else {
                                this.male.setSelected(false);
                                this.female.setSelected(true);
                            }

                            contactPane.setVisible(true);
                        }
                    };

                    tree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventEventHandler);

                    // Création from load
                    if (selectedItem == null) {
                        TreeItem<Object> parent = rootNode.getChildren().get(0);
                        parent.getChildren().add(newContact);
                    }

                    // Création manuelle
                    else if (selectedItem.getValue() instanceof Group) {
                        selectedItem.getChildren().add(newContact);
                    } else {
                        // Edition contact
                        selectedItem.getParent().getChildren().add(newContact);
                    }
                });
            }

            if (change.wasRemoved()) {
                change.getRemoved().forEach(c -> {
                    TreeItem<Object> contactItem = contactToTreeItem(selectedItem.getParent(), c);
                    selectedItem.getParent().getChildren().remove(contactItem);
                });
            }
        };

        // Add group or contact
        add.setOnAction(evt -> {
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();

            // Add new group
            if (selectedItem.getValue() == rootNode.getValue()) {

                // Create group
                Group group = new Group();
                model.getGroups().add(group);

                // Bind listener
                group.getContacts().addListener(contactsListener);
            }

            // Fill info of new contact
            else if (selectedItem.getValue() instanceof Group) {
                contactPane.visibleProperty().set(true);
                this.firstName.clear();
                this.lastName.clear();
                this.addressName.clear();
                this.postcode.clear();
                this.city.clear();
                this.birthDate.getEditor().clear();
                this.male.setSelected(false);
                this.female.setSelected(false);
            }

        });

        // Remove group or contact
        remove.setOnAction(evt -> {
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();

            // Remove group
            if (selectedItem.getValue() instanceof Group) {
                model.getGroups().remove(selectedItem.getValue());
            }

            else if (selectedItem.getValue() instanceof Contact) {
                Group parent = (Group) selectedItem.getParent().getValue();
                parent.getContacts().remove(selectedItem.getValue());
            }
        });

        // groupsListener
        groupsListener = change -> {
            change.next();
            if(change.wasAdded()) {
                change.getAddedSubList().forEach(g -> {
                    TreeItem<Object> newGroup = new TreeItem(g, new ImageView(new Image("Images/group.png")));
                    newGroup.setExpanded(true);
                    rootNode.getChildren().add(newGroup);
                });
            }

            if (change.wasRemoved()) {
                change.getRemoved().forEach(g -> {
                    TreeItem<Object> groupItem = groupToTreeItem(rootNode, g);
                    rootNode.getChildren().remove(groupItem);
                });
            }
        };

        this.model.getGroups().addListener(groupsListener);

        // Save
        save.setOnAction(evt -> {
            try {
                FileOutputStream file = new FileOutputStream("save");
                ObjectOutputStream oos = new ObjectOutputStream(file);
                for (Group g : this.model.getGroups()) {
                    oos.writeObject(g);
                    oos.writeObject(new ArrayList<Contact>(g.getContacts()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        //Load
        load.setOnAction(evt -> {
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(new FileInputStream("save"));
                while (true) {
                    Group g = (Group) ois.readObject();
                    this.model.getGroups().add(g);
                    g.getContacts().addListener(contactsListener);
                    List<Contact> contacts = (List<Contact>) ois.readObject();
                    for (Contact c : contacts) {
                        out.println(c.toString());
                        g.getContacts().add(c);
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });

    }

    public TreeItem<Object> groupToTreeItem(TreeItem<Object> root, Group g){
        return root.getChildren()
                .stream()
                .filter(f ->  g.equals(f.getValue()))
                .findFirst()
                .get();
    }

    public TreeItem<Object> contactToTreeItem(TreeItem<Object> g, Contact c){
        return g.getChildren()
                .stream()
                .filter(f ->  c.equals(f.getValue()))
                .findFirst()
                .get();
    }


}