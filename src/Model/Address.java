package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Address {

    private StringProperty addressName;
    private StringProperty postcode;
    private StringProperty city;
    private StringProperty country;

    public String getAddressName() {
        return addressName.get();
    }

    public StringProperty addressNameProperty() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName.set(addressName);
    }

    public String getPostcode() {
        return postcode.get();
    }

    public StringProperty postcodeProperty() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode.set(postcode);
    }

    public String getCity() {
        return city.get();
    }

    public StringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getCountry() {
        return country.get();
    }

    public StringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }

    public Address() {
        this.addressName = new SimpleStringProperty(this, "addressName", null);
        this.postcode = new SimpleStringProperty(this, "postcode, null");
        this.city = new SimpleStringProperty(this, "city", null);
        this.country = new SimpleStringProperty(this, "country", null);
    }
}
