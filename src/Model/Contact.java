package Model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.LocalDate;

import static java.lang.System.out;

public class Contact implements Cloneable, Externalizable {

    private StringProperty lastName;
    private StringProperty firstName;
    private Address address;
    private ObjectProperty<LocalDate> birthDate;
    private StringProperty sex;

    private ObservableMap<String,String> errors;

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public LocalDate getBirthDate() {
        return birthDate.get();
    }

    public ObjectProperty<LocalDate> birthDateProperty() {
        return birthDate;
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public Address getAddress() {
        return address;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate.set(birthDate);
    }

    public String getSex() {
        return sex.get();
    }

    public StringProperty sexProperty() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public Contact() {
        this.lastName = new SimpleStringProperty(this, "lastName", null);
        this.firstName = new SimpleStringProperty(this, "firstName", null);
        this.address = new Address();
        this.birthDate = new SimpleObjectProperty<LocalDate>(this, "birthDate", null);
        this.sex = new SimpleStringProperty(this, "sex", null);

        this.errors = FXCollections.observableHashMap();
    }

    public ObservableMap<String, String> getErrors() {
        return errors;
    }

    public Boolean validate() {

        Boolean correct = true;

        if (this.getLastName().equals("")) {
            errors.put(lastName.getName(), "Lastname is mandatory");
            correct = false;
        } else if (errors.get(lastName.getName()) != null) {
            errors.remove(lastName.getName());
        }

        if (this.getFirstName().equals("")) {
            errors.put(firstName.getName(), "Firstname is mandatory");
            correct = false;
        } else if (errors.get(firstName.getName()) != null) {
            errors.remove(firstName.getName());
        }

        if (this.getAddress().getAddressName().equals("")) {
            errors.put(this.getAddress().addressNameProperty().getName(), "Address name is mandatory");
            correct = false;
        } else if (errors.get(this.getAddress().addressNameProperty().getName()) != null) {
            errors.remove(this.getAddress().addressNameProperty().getName());
        }

        if (this.getAddress().getPostcode().equals("")) {
            errors.put(this.getAddress().postcodeProperty().getName(), "Postcode is mandatory");
            correct = false;
        } else if (errors.get(this.getAddress().postcodeProperty().getName()) != null) {
            errors.remove(this.getAddress().postcodeProperty().getName());
        }

        if (this.getAddress().getCity().equals("")) {
            errors.put(this.getAddress().cityProperty().getName(), "City is mandatory");
            correct = false;
        } else if (errors.get(this.getAddress().cityProperty().getName()) != null) {
            errors.remove(this.getAddress().cityProperty().getName());
        }

        if (this.getAddress().countryProperty().getValue() == null) {
            errors.put(this.getAddress().countryProperty().getName(), "Country is mandatory");
            correct = false;
        } else if (errors.get(this.getAddress().countryProperty().getName()) != null) {
            errors.remove(this.getAddress().countryProperty().getName());
        }

        if (this.birthDate.getValue() == null) {
            errors.put(birthDate.getName(), "Birthdate is mandatory");
            correct = false;
        } else if (errors.get(birthDate.getName()) != null) {
            errors.remove(birthDate.getName());
        }

        if (this.sex.getValue() == null) {
            errors.put(sex.getName(), "Sex is mandatory");
            correct = false;
        } else if (errors.get(sex.getName()) != null) {
            errors.remove(sex.getName());
        }

        return correct;

    }

    public Contact clone() throws CloneNotSupportedException {
        Contact clone = new Contact();
        clone.setFirstName(this.getFirstName());
        clone.setLastName(this.getLastName());
        clone.setBirthDate(this.getBirthDate());
        clone.setSex(this.getSex());
        clone.getAddress().setCountry(this.getAddress().getCountry());
        clone.getAddress().setAddressName(this.getAddress().getAddressName());
        clone.getAddress().setCity(this.getAddress().getCity());
        clone.getAddress().setPostcode(this.getAddress().getPostcode());

        return clone;
    }

    @Override
    public String toString() {
        return getFirstName();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(this.getFirstName());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        firstName.set(in.readUTF());
    }
}
